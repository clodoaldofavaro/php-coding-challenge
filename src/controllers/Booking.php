<?php

namespace Src\controllers;

use Src\models\BookingModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

	public function createBooking(int $clientId, string $checkinDate, string $checkoutDate, int $price) {
		return $this->getBookingModel()->createBooking($clientId, $checkinDate, $checkoutDate, $price);
	}
}