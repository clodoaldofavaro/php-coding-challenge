<?php

namespace Src\models;

use DateTime;
use Exception;
use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
	private $helper;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
		$this->helper = new Helpers();
	}

	public function getBookings() {
		return $this->bookingData;
	}

	public function createBooking(int $clientId, string $checkinDate, string $checkoutDate, int $price) {
		$clientModel = new ClientModel();
		$client = $clientModel->getClientById($clientId);

		if (!$client) {
			return ['error' => 'Client not found.'];
		}

		try {
			$startDate = new DateTime($checkinDate);
			$endDate = new DateTime($checkoutDate);
		} catch (Exception $e) {
			return ['error' => 'Invalid date format.'];
		}

		$bookings = $this->getBookings();

		$newBooking = [
			'id' => end($bookings)['id'] + 1,
			'clientId' => $clientId,
			'price' => $price,
        	'checkindate' => $checkinDate,
        	'checkoutdate' => $checkoutDate
		];

		$bookings[] = $newBooking;

		$this->helper->putJson($bookings, 'bookings');

		return $newBooking;
	}
}